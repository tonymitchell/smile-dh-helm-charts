# [1.0.0-pre.45](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/compare/v1.0.0-pre.44...v1.0.0-pre.45) (2023-02-09)


### Features

* **smilecdr:** add license support ([3c429d8](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/commit/3c429d822754bceccebfb1783120b876cbcfc8b1))

# [1.0.0-pre.44](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/compare/v1.0.0-pre.43...v1.0.0-pre.44) (2023-02-09)


### Bug Fixes

* **smilecdr:** fix paths for initcontainer ([ce17551](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/commit/ce17551b893d14a613ffad8875839ff5086f3f07))

# [1.0.0-pre.43](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/compare/v1.0.0-pre.42...v1.0.0-pre.43) (2023-01-30)


### Features

* **smilecdr:** add support for 2023.02 release ([7c583de](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/commit/7c583dec7a70a9ba7f0677df1afcb175a2386f49))

# [1.0.0-pre.42](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/compare/v1.0.0-pre.41...v1.0.0-pre.42) (2023-01-30)


### Features

* **smilecdr:** add config locking options ([351d48d](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/commit/351d48d5425ddac08d3e4601843ca281191bf7e2))

# [1.0.0-pre.41](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/compare/v1.0.0-pre.40...v1.0.0-pre.41) (2023-01-28)


### Features

* **smilecdr:** add support for Alpine3 base image ([dc7e960](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/commit/dc7e960d3978723238fbd9e91bee565385b2a6f6))

# [1.0.0-pre.40](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/compare/v1.0.0-pre.39...v1.0.0-pre.40) (2023-01-28)


### Bug Fixes

* **smilecdr:** fix ingress annotation overrides ([78d254e](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/commit/78d254e3f891a2ae03b9f181a2da9e965017269c))

# [1.0.0-pre.39](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/compare/v1.0.0-pre.38...v1.0.0-pre.39) (2023-01-27)


### Bug Fixes

* **smilecdr:** fix key names in env vars ([81defae](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/commit/81defaef33a5fa1f1cadfc4b14f68896f6b7cbff))

# [1.0.0-pre.38](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/compare/v1.0.0-pre.37...v1.0.0-pre.38) (2023-01-27)


### Bug Fixes

* **smilecdr:** fix key names in k8s secret ([b84760a](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/commit/b84760aa4df7999c5c151eda437649511c11d88b))

# [1.0.0-pre.37](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/compare/v1.0.0-pre.36...v1.0.0-pre.37) (2023-01-26)


### Bug Fixes

* **smilecdr:** change objectAlias naming logic ([ade3f22](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/commit/ade3f22fde28f2d32bbc6699cceb1697b866a33a))

# [1.0.0-pre.36](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/compare/v1.0.0-pre.35...v1.0.0-pre.36) (2023-01-11)


### Features

* **smilecdr:** update ingress logic and docs ([affff39](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/commit/affff3956865d817f14988ea420bc61e1b5f38ad))

# [1.0.0-pre.35](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/compare/v1.0.0-pre.34...v1.0.0-pre.35) (2023-01-06)


### Features

* **smilecdr:** copy files from external location ([ea2710f](https://gitlab.com/smilecdr-public/smile-dh-helm-charts/commit/ea2710f406d43b610e7cfa41b2d8e9859f9536d2))

# [1.0.0-pre.34](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.33...v1.0.0-pre.34) (2022-12-22)


### Bug Fixes

* **smilecdr:** fix ConfigMap reference in volume ([5cc81e8](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/5cc81e8c016d7d708ab8d8df39b147665406eec4))

# [1.0.0-pre.33](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.32...v1.0.0-pre.33) (2022-12-22)


### Bug Fixes

* **smilecdr:** force lower case in resource names ([6d1e4aa](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/6d1e4aa1ff27d7541f370859651047065eece060))

# [1.0.0-pre.32](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.31...v1.0.0-pre.32) (2022-12-20)


### Features

* **smilecdr:** change db secret config ([8069b77](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/8069b77d4920ef64154eca12d06c4c788241a831))


### BREAKING CHANGES

* **smilecdr:** Values file needs to be updated if using sscsi for DB
secrets

# [1.0.0-pre.31](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.30...v1.0.0-pre.31) (2022-12-14)


### Features

* **smilecdr:** disable crunchypgo ([a2a4e32](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/a2a4e32f624a5d348d8bf03c5ebcd90dea8dd690))

# [1.0.0-pre.30](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.29...v1.0.0-pre.30) (2022-12-10)


### Features

* **smilecdr:** add argocd feature ([75a4874](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/75a4874bd2da5dede8630babe586143f4f1848b1))

# [1.0.0-pre.29](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.28...v1.0.0-pre.29) (2022-12-10)


### Bug Fixes

* **smilecdr:** fix secret reference keys ([2f6411b](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/2f6411ba5b81fc7139d1335b6b4935405e27556f))

# [1.0.0-pre.28](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.27...v1.0.0-pre.28) (2022-12-06)


### Features

* **smilecdr:** make image secret required ([e068330](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/e068330a9e2b1b890b6008ab5790a5bfa0180c91))

# [1.0.0-pre.28](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.27...v1.0.0-pre.28) (2022-12-05)


### Features

* **smilecdr:** make image secret required ([2c2389b](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/2c2389be7a1533c951c52246de852fe81a7585a0))

# [1.0.0-pre.27](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.26...v1.0.0-pre.27) (2022-12-03)


### Bug Fixes

* **smilecdr:** fix DB_PORT in default modules ([2239f60](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/2239f60ec6d3ea5bc21e77c20431bca03a0dae20))

# [1.0.0-pre.26](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.25...v1.0.0-pre.26) (2022-12-03)


### Bug Fixes

* **smilecdr:** correct the field for image secrets ([2ddeef2](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/2ddeef2a10f176e231130e01c8bdc485875aad73))

# [1.0.0-pre.25](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.24...v1.0.0-pre.25) (2022-12-02)


### Features

* **smilecdr:** support multiple databases ([e28bb13](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/e28bb131b5976e705eed227a6efd3b4cd48c325e))

# [1.0.0-pre.24](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.23...v1.0.0-pre.24) (2022-12-02)


### Bug Fixes

* **smilecdr:** improve modules include logic ([f7850d2](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/f7850d2d47dacd331b902f2cf4c2295e4b385265))

# [1.0.0-pre.23](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.22...v1.0.0-pre.23) (2022-12-02)


### Bug Fixes

* **smilecdr:** allow quoted config entries ([f9d7e1f](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/f9d7e1f9b420926ee0a2d4a7db0960c10f8cf61d))

# [1.0.0-pre.22](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.21...v1.0.0-pre.22) (2022-12-01)


### Bug Fixes

* **smilecdr:** change crunchydata resource names ([654417b](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/654417b7a9dce46f84947d371fa4426b03bf7783))


### Features

* **smilecdr:** improve CrunchyData integration ([4289432](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/428943258417ada4c1dcdd7e9d75bb6cee0139fd))

# [1.0.0-pre.21](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.20...v1.0.0-pre.21) (2022-12-01)


### Features

* **smilecdr:** update JVM tuning params ([cc1f859](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/cc1f8591deb55f9491892995b632a45af44c0d84))

# [1.0.0-pre.20](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.19...v1.0.0-pre.20) (2022-12-01)


### Bug Fixes

* **smilecdr:** remove image reference ([2b47fb5](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/2b47fb56be3397e0cf19899c0d5442fce6419311))

# [1.0.0-pre.19](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.18...v1.0.0-pre.19) (2022-12-01)


### Features

* **smilecdr:** add support for strimzi kafka ([27b8fb4](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/27b8fb4a4d61babec1934024874a4b4a2276b4a2))

# [1.0.0-pre.18](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.17...v1.0.0-pre.18) (2022-12-01)


### Features

* **smilecdr:** add autoscaling support ([6ff0f2f](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/6ff0f2fb33a1df6cd94d22872b5b67e8cf788120))

# [1.0.0-pre.17](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.16...v1.0.0-pre.17) (2022-12-01)


### Features

* **smilecdr:** configure rolling deployments ([daa9c4b](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/daa9c4b43ca9f59b1e26dc7a4a3a1da62d859e1d))

# [1.0.0-pre.16](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.15...v1.0.0-pre.16) (2022-12-01)


### Features

* **smilecdr:** add pod disruption budget ([b8b46e4](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/b8b46e47213a40d29a47020aa3d18c07f4f88558))

# [1.0.0-pre.15](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.14...v1.0.0-pre.15) (2022-12-01)


### Features

* **smilecdr:** add redeploy on config changes ([90e33ad](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/90e33ad5e725957e3c1cd039487e19e88548618d))

# [1.0.0-pre.14](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.13...v1.0.0-pre.14) (2022-11-30)


### Features

* **smilecdr:** add readiness probe ([5a03100](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/5a031005cc9fda8622bc1c2b643d1e3ba156ad6c))
* **smilecdr:** add startup probe ([65014ad](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/65014ad33e4225ece5106fd2d2485f1cd18816d6))

# [1.0.0-pre.13](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.12...v1.0.0-pre.13) (2022-11-25)


### Features

* **smilecdr:** update CDR version and modules ([116e187](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/116e187150326b59d974c2f2624c2a7daa922f12))


### BREAKING CHANGES

* **smilecdr:** - This updates the Smile CDR version

# [1.0.0-pre.12](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.11...v1.0.0-pre.12) (2022-11-25)


### Features

* **smilecdr:** add support for deploying postgres ([0fc81b1](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/0fc81b1d99c95c3e319cab859d52af07774b7429))

# [1.0.0-pre.11](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.10...v1.0.0-pre.11) (2022-11-24)


### Bug Fixes

* **smilecdr:** fix reference to configmap data ([383fdd6](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/383fdd6f18780df4ad3f1b1a7a64fcf10c3ff379))

# [1.0.0-pre.10](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.9...v1.0.0-pre.10) (2022-11-24)


### Features

* **smilecdr:** support injecting files ([a823a7d](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/a823a7dd8f5b331344fdbd554d899c52e7db55ee))

# [1.0.0-pre.9](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.8...v1.0.0-pre.9) (2022-11-23)


### Features

* **smilecdr:** automate setting JVMARGS ([6fcda8b](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/6fcda8b86ff8a67198b8fa208167ee9f6c5636f6))

# [1.0.0-pre.8](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.7...v1.0.0-pre.8) (2022-11-22)


### Features

* **smilecdr:** set default replicas to 1 ([9788832](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/97888327632c91dccc784a3e4891d9fcad5d58dc))

# [1.0.0-pre.7](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.6...v1.0.0-pre.7) (2022-11-22)


### Features

* **smilecdr:** add name override for CrunchyPGO ([110e133](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/110e1336c309adb3602f9103421f0693ca32424d))

# [1.0.0-pre.6](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.5...v1.0.0-pre.6) (2022-11-22)


### Features

* **smilecdr:** add Secrets Store CSI support ([f8f23ba](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/f8f23ba2e3e8b6a87fc80d7cf04a7ae9a221782f))

# [1.0.0-pre.5](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.4...v1.0.0-pre.5) (2022-11-21)


### Features

* **smilecdr:** add support for IRSA (IAM roles) ([509bbe3](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/509bbe389df8e2c908161eb11e9fca7a1df15755))

# [1.0.0-pre.4](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.3...v1.0.0-pre.4) (2022-11-21)


### Features

* **smilecdr:** update Ingress definition logic ([2271d57](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/2271d57ff5bd8aef792ca4310df86eb5913682cf))


### BREAKING CHANGES

* **smilecdr:** Now uses `nginx-ingress` instead of
`aws-lbc-nlb` for specifying Nginx Ingress Controller

# [1.0.0-pre.3](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.2...v1.0.0-pre.3) (2022-11-21)


### Features

* **smilecdr:** add back default tag functionality ([46785e5](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/46785e5ee54087a8ec4df2139f9630827f655d81))
* **smilecdr:** add common labels to all resources ([618ba2e](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/618ba2ed8a96fda736d52dc50519485237dbede8))
* **smilecdr:** normalize resource names ([6ca25bd](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/6ca25bdec998a081aa114eb598702ee6a1819570))
* **smilecdr:** remove extra labels from default values ([d971934](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/d9719344f890baaf8b939f1ef3d641f959775f22))

# [1.0.0-pre.2](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/compare/v1.0.0-pre.1...v1.0.0-pre.2) (2022-11-21)


### Features

* **smilecdr:** remove hard coded entries from ConfigMap ([b6a2fb5](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/b6a2fb56ae9324ef08976cf92487ba7b7f1e7f2c))

# 1.0.0-pre.1 (2022-11-21)


### Features

* **repo:** initial Commit ([5f98460](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/5f9846020a2da8d343f55e36e3fa896206717ef8)), closes [#68834381](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/issues/68834381)
* **smilecdr:** add external module files support ([da374c1](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/da374c1a97735a895c27356025650d00da2db4c8))
* **smilecdr:** update application version ([9b203f2](https://gitlab.com/smilecdr-techops/smile-dh-helm-charts/commit/9b203f277955fb34831592fe064f19863765b2a1))
