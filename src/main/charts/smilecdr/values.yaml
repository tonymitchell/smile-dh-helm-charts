## @section Image parameters
## @param image.repository OCI repository with Smile CDR images
## @param image.tag Smile CDR version to install. Default is the chart appVersion.
## @skip image.pullPolicy
image:
  # -- Smile CDR version to install. Default is the chart appVersion.
  tag: ""
  # -- OCI repository with Smile CDR images
  repository: docker.smilecdr.com/smilecdr
  # -- Image Pull Policy
  pullPolicy: IfNotPresent

  ## @section Image Credentials
  ## @param image.credentials.type Type of credentials. `sscsi`, `extsecret` or `values`

  ## @section Image Credentials (values)
  ## @param image.credentials.values[0].registry OCR registry these values are for
  ## @param image.credentials.values[0].username OCR registry username
  ## @param image.credentials.values[0].password OCR registry password

  # -- You must provide image credentials of type `sscsi`, `extsecret` or `values`
  credentials: {}
    # Choose one of sscsi, extsecret, values
    # type: sscsi
    # provider: aws
    # secretarn: "arn:aws:secretsmanager:us-east-1:1234567890:secret:secretname"

    # type: extsecret
    # pullSecrets:
    #   - name: scdr-docker-secrets

    # type: values
    # values:
    # - registry: docker.com
    #   username: user
    #   password: pass

## @section Public URL
## @param specs.hostname Hostname for Smile CDR instance
## @param specs.rootPath Root pathname
specs:
  # -- Hostname for Smile CDR instance
  hostname: smilecdr-example.local
  # Define a common root context path, if required.
  rootPath: /

## @section Resources
## @param.requests.cpu CPU Requests
## @param.limits.memory Memory allocation
## @param.jvm.memoryFactor JVM HeapSize factor. `limits.memory` is multiplied this to calculate `-Xmx`
## @param.jvm.xms Set JVM heap `-Xms` == `-Xmx`
## @param.jvm.args Set extra JVM args
resources:
  # As Smile CDR is a high performance Java based application, special consideration needs to be given
  # to the resource settings and JVM tuning parameters.
  # Typical cloud best practices are to start small and increase as workload increases. This can be a
  # challenging approach with an appliction such as Smile CDR if we are to avoid OOM Killed errors.
  # You should start with a somewhat larger memory allocation and perform monitoring of your workload
  # to determine real life memory usage. Then tune-down to a size that still gives a reasonable buffer
  # for any workload spikes.
  # These settings go hand-in-hand with the jvm.memoryFactor and jvm.xmx settings as any Java based
  # application will use more memory than specified in -Xmx alone. See the jvm section for more detail
  # on this.
  requests:
    # -- CPU Requests
    cpu: "1"
    # As per best practice, limits.memory == requests.memory
    # This is easily achieved by only setting limits.memory
    # If requests.memory is undefined, Kubernetes sets it to match limits.memory
  limits:
    # As per best practice, do not set limits.cpu
    # This approach may be changed if there are per-core licensing concerns
    # Note that this will cause a warning in some linters
    # -- Memory allocation
    memory: 4Gi

jvm:
  # As Smile CDR is a Java application, we need to pay special attention to the
  # JVM memory settings and tuning.
  # A Java application will use more memory than is specified in the `-Xmx` option,
  # so setting it to the same as the l`imits.memory` setting almost guarantees that
  # the pod will be evicted at some point with an OOM Killed error.
  # The exact factor can be hard to determine as it will depend on factors such as
  # the module configurations, required workload throughput and horizontal scaling
  # factors.
  # The `jvm.memoryFactor` setting will set `-Xmx` based on `resources.memory` if
  # explicitly set, or fall back to `limits.memory` (Which ends up being the same
  # as `requests.memory` if it was unset)
  # For example:
  #  * A setting of 0.5 means the `-Xmx` will be set to `0.5 * resources.memory` or
  #    `0.5 * limits.memory`
  #  * If `limits.memory` is set to `8Gi`, `-Xmx` will be set to `4096m`
  #  * If `limits.memory` is set to `8Gi` and `resources.memory` is set to `4Gi`,
  #    `-Xmx` will be set to `2048m`
  # Conversion of Kubernetes style suffix to Java style suffixes is performed
  # automatically
  # -- JVM HeapSize factor. `limits.memory` is multiplied this to calculate `-Xmx`
  memoryFactor: 0.5

  # If you have your memory settings set such that an OOM Kill event is likely as the
  # JVM Heap grows, it may not become apparent until you reach some high workload
  # spike, or after the heap grows over time. In these cases it can be troublesome
  # to determine the cause. This is much more relevant in a K8s Pod scenario than when
  # running directly on a VM, as there is no swap space enabled to "absorb" this memory groth.
  # It is recommended to set `-Xms`` to the same value as `-Xmx``. This way, any insufficient
  # memory settings may be surfaced sooner. It results in a more stable and predictable
  # environment.
  # -- Set JVM heap `-Xms` == `-Xmx`
  xms: true

  # -- Set extra JVM args
  args:
  - "-Dsun.net.inetaddr.ttl=60"
  - "-Djava.security.egd=file:/dev/./urandom"


## @section Scaling
## @param replicaCount Number of replicas to deploy. Note that this setting is ignored if autoscaling is enabled. Should always start a new installation with 1
## @param autoscaling.enabled Enable or disable autoscaling
## @param autoscaling.minReplicas Recommend 1 for dev environments, 2 for prod or 3 for HA prod
## @param autoscaling.maxReplicas Depends on peak workload requirements and available licensing
## @param autoscaling.targetCPUUtilizationPercentage Tune this value to alter autoscaling behaviour

# -- Number of replicas to deploy. Note that this setting is ignored if autoscaling is enabled. Should always start a new installation with 1
replicaCount: 1

autoscaling:
  # -- Enable or disable autoscaling
  enabled: false
  # -- Recommend 1 for dev environments, 2 for prod or 3 for HA prod
  minReplicas: 1
  # -- Depends on peak workload requirements and available licensing
  maxReplicas: 4
  targetCPUUtilizationPercentage: 80

## @section External Database Configuration
## @param database.external.enabled Enable database external to K8s cluster
## @param database.external.credentials Source of DB secret for external database. A map with `type` set to `sscsi` or `extsecret` and `provider` set to `aws`
## @param database.external.databases List of databases used by this Smile CDR installation
## @param database.external.databases.secretName Name of K8s secret with DB connection credentials
## @param database.external.databases.module The module that this database is used for
## @skip database.external.databases.url
## @skip database.external.databases.dbType
## @skip database.external.databases.dbname
## @skip database.external.databases.user
## @skip database.external.databases.passKey

database:
  # Either crunchypgo or external should be set to true. Not both.
  # If using external DB, then Kubernetes secret needs to be set up with
  # the correct credentials. See docs.
  external:
    # -- Enable database external to K8s cluster
    enabled: false
    credentials: {}
    # Choose `sscsi` or `extsecret`
    # type: sscsi
    # provider: aws
    # @skip databases
    databases:
    - secretName: smilecdr
      module: clustermgr
      # You can specify the key name to extract the value from the secret
      urlKey: url
      portKey: port
      dbnameKey: dbname
      userKey: user
      passKey: password

      # Alternatively, you can specify values for connection parameters.
      # Any values specified here will take priority over parameters from
      # secret keys, except the password, which MUST come from the secret key.
      # url: "db-url"
      # port: 5432
      # dbname: cdr
      # user: username
      # passKey: password

  ## @section CrunchyData PGO Database Configuration
  ## @param database.crunchypgo.enabled Enable database provisioned in-cluster via CrunchyData PGO
  ## @param database.crunchypgo.users List of PostgreSQL users/databases to create.
  ## @param database.crunchypgo.users[1].name PostgreSQL username
  ## @param database.crunchypgo.users[1].module Smile CDR module that will use this user/database
  ## @param database.crunchypgo.internal Create the Postgres database as part of this Helm Chart
  ## @param database.crunchypgo.config.postgresVersion PostgreSQL version to use
  ## @param database.crunchypgo.config.instanceReplicas Number of Postgres instances to run (For HA)
  ## @param database.crunchypgo.config.instanceCPU PostgrSQL cpu allocation
  ## @param database.crunchypgo.config.instanceMemory PostgrSQL memory allocation
  ## @param database.crunchypgo.config.instanceSize PostgrSQL storage allocation
  ## @param database.crunchypgo.config.backupsSize PostgrSQL backups storage allocation
  ##

  crunchypgo:
    # -- Enable database provisioned in-cluster via CrunchyData PGO
    enabled: false
    # -- Create the Postgres database as part of this Helm Chart
    internal: false

    config:
      # -- PostgreSQL version to use
      postgresVersion: 14
      # -- Number of Postgres instances to run (For HA)
      instanceReplicas: 2
      # -- PostgrSQL cpu allocation
      instanceCPU: 1
      # -- PostgrSQL memory allocation
      instanceMemory: 2Gi
      # -- PostgrSQL storage allocation
      instanceSize: 10Gi
      # -- PostgrSQL backups storage allocation
      backupsSize: 10Gi
      # If you need faster disk for the PostgreSQL cluster, you can specify a higher performance
      # `storageClass` in your cluster and reference it here.
      #storageClass: gp3-fast
    users:
      # - PostgreSQL username
    - name: smilecdr
      # -- Smile CDR module that will use this user/database
      module: clustermgr

## @skip cdrNodes
# This is where we define multiple node configurations for Smile CDR
# This is a developing feature and there may be breaking changes later on
# For now, it only defines the name, which is used in the properties ConfigMap
# It may later be used to define deployment-specific attributes.
# Do not change any of these values.
# @ignored
cdrNodes:
  Masterdev:
    enabled: true # Not used yet.
    logsDirSize: 10Gi
    config:
      locked: true
      # This option lets you update configs in the console for troubleshooting/testing/experimenting.
      # Your changes will be lost if the pod restarts or if another pod joins the cluster.
      # It sets `config.Locked` to `false` and sets `node.propertysource` to `PROPERTIES_UNLOCKED`
      # See the [Smile CDR Docs](https://smilecdr.com/docs/installation/installing_smile_cdr.html#module-property-source) for more info
      troubleshooting: false
    security:
      strict: false

## @section Smile CDR Modules Configuration
## @param modules.usedefaultmodules Enable or disable included default modules configuration
## @param autoDeploy Enable or disable automatic deployment of changes to Smile CDR configuration
# Define modules to be used. Some of these will contain service definitions.
# A service and an ingress rule will be created for modules that use services.
# Canonical endpoint URLs will be generated by _smile-module-helpers.tpl and
# populated in the smilecdr.services variable. These can be consumed by other
# modules that reference them.
modules:
  # -- Enable or disable included default modules configuration
  usedefaultmodules: true

# Automatically do rolling deployment when configmaps or files are changed
# -- Enable or disable automatic deployment of changes to Smile CDR configuration
autoDeploy: true

## @section Including Extra Files into Smile CDR
## @param Map of file definitions to map into the Smile CDR instance
# -- Map of file definitions to map into the Smile CDR instance
mappedFiles: {}
# This needs to be used in conjunction with the --set-file option of Helm
#   testfile.txt:
#     type: configMap
#     configMapBaseName: smilecdr
#     path: /home/smile/smilecdr/classes

## @section Copying Extra Files into Smile CDR Pods from external location
copyFiles: {}
##  Copies files from S3 to the classes directory
#   classes:
#     syncDefaults: true
#     source:
#       type: s3
#       bucket: s3-bucket-name
#       path: /version/classes
##  Copies files from S3 to the customerlib directory
#   customerlib:
#     source:
#       type: s3
#       bucket: s3-bucket-name
#       path: /version/customerlib

## @section Ingress
## @param ingress.enabled Enable Ingress
## @param ingress.type Ingress type (`nginx-ingress`,`aws-lbc-alb`,`azure-appgw`)
## @skip ingress.extraAnnotations
##
ingress:
  # -- Enable Ingress
  enabled: true
  # Select the cloud vendor and GW/NLB controller being used.
  # Currently supported values are:
  # nginx-ingress - Nginx Ingress Controller (Network Load Balancer when using AWS Load Balancer Controller)
  # aws-lbc-alb - AWS Load Balancer Controller (Application Load Balancer)
  # azure-appgw - Azure Application Gateway
  # other - If it is none of the above, or unset, no default Ingress annotations will be created. It is up to
  #         you to provide appropriate `extraAnnotations` below
  # -- Ingress type (`nginx-ingress`,`aws-lbc-alb`,`azure-appgw`)
  type: nginx-ingress

  # This chart assumes certain default Ingress Class names. If your environment has defined a different class
  # name then override it here
  # ingressClassNameOverride: my-nginx

  # Define any extra ingress annotations here.
  # @ignore
  annotations: {}

## @section Service Account (IRSA)
## @param serviceAccount.create Specifies whether a service account should be created
## @param serviceAccount.annotations Annotations to add to the service account
## @param serviceAccount.name Autogenerated if not set
serviceAccount:
  # -- Specifies whether a service account should be created
  create: false
  # -- Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  # -- Autogenerated if not set
  name: ""

## @section messageBroker Message Broker Configuration
## @param messageBroker.external.enabled Enable external Message Broker
## @param messageBroker.external.type External message broker type
## @param messageBroker.external.bootstrapAddress External message broker bootstrap address
## @param messageBroker.external.tls External message broker TLS support
## @param messageBroker.strimzi.enabled Enable provisioning of Kafka using Strimzi Operator
## @skip messageBroker.strimzi.config

## @param.messageBroker.channelPrefix Topic Channel Prefix

messageBroker:
  # -- Topic Channel Prefix
  channelPrefix: SCDR-ENV-
  external:
    # -- Enable external message broker
    enabled: false
    # -- External message broker type
    type: kafka
    # -- External message broker bootstrap address
    bootstrapAddress: kafka-example.local
    # -- External message broker TLS support
    tls: true
  strimzi:
    # -- Enable provisioning of Kafka using Strimzi Operator
    enabled: false
    # @ignore
    config:
      version: "3.3.1"
      protocolVersion: "3.3"
      tls: true
      kafka:
        replicas: 3
        volumeSize: 10Gi
        resources:
          requests:
            cpu: "0.5"
            memory: 1Gi
          limits:
            memory: 1Gi
      zookeeper:
        replicas: 3
        volumeSize: 10Gi
        resources:
          requests:
            cpu: 0.5
            memory: 512Mi
          limits:
            memory: 512Mi

## @section Extra labels
## @param labels Extra labels to apply to all resources
# Extra labels
# -- Extra labels to apply to all resources
labels: {}

## @skip podAnnotations
# @ignored
podAnnotations: {}

## @skip podSecurityContext
# @ignored
podSecurityContext: {}

## @skip securityContext
# @ignored
securityContext:
  capabilities:
    drop:
    - ALL
  privileged: false
  allowPrivilegeEscalation: false
  runAsNonRoot: true
  runAsUser: 1000
  readOnlyRootFilesystem: true

## @skip nodeSelector
# @ignored
nodeSelector: {}

## @skip tolerations
# @ignored
tolerations: []

## @skip affinity
# @ignored
affinity: {}
