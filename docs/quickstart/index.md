# Deployment Quickstart
This section of the documentation will get you up and running quickly to show how
the chart works. For any real deployments, please look through the advanced
deplopyments in the [User Guide](../guide/smilecdr/index.md) and
[Examples](../examples/index.md) sections to design a solution that works for your environment.

## Preparation
To deploy Smile CDR using these Helm Charts, you will need to do the following:

* Ensure all requirements and dependencies are in place
* Prepare a set of configurations to suit your planned installation architecture
* Perform the deployment

The following pages will guide you through the above steps to so that you can gain familiarity with
how these Helm Charts function.

## Advanced Deployment

The Quickstart shows you a basic install that does not follow security best practices.
To install with best practices in mind, refer to the advanced configurations in the
[User Guide](../guide/smilecdr/index.md) section which goes into detail on all
available configuration options.
