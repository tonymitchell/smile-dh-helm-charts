# Overview

This is the documentation for the Helm Charts provided by Smile Digital Health.

These Helm Charts are provided to simplify installation and configuration of Smile Digital Health
products in Kubernetes environments using a number of best practices.

Currently, the only chart available is for the core [Smile CDR](https://www.smilecdr.com/smilecdr) product.

>**WARNING: These charts are still in pre-release!**<br>
As such, there may still be **breaking changes** introduced without notice.

Only use this version of the chart for evaluation or testing.

## Getting Started

To quickly see these charts in action, follow the guide in the [Quickstart](quickstart/index.md) section.

## Configuration

Full details on configuring the products using these charts are provided in the
[User Guide](guide/index.md) section.
