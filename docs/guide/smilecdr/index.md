# Smile CDR Helm Chart User Guide

This guide covers configuration details for the Smile CDR Helm Chart

## Smile CDR Configuration Options

Please note that for details on the Smile CDR configuration options, you should
consult the official product documentation on the [Smile CDR website](https://smilecdr.com/docs)
